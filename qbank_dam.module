<?php

/**
 * @file
 * Contains qbank_dam.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_help().
 */
function qbank_dam_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the qbank_dam module.
    case 'help.page.qbank_dam':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Integration between drupal and QBank3.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_library_info_alter().
 */
function qbank_dam_library_info_alter(&$libraries, $extension) {
  if ($extension == 'qbank_dam') {
    $protocol = \Drupal::service('qbank_dam.service')->getProtocol();
    $url      = \Drupal::service('qbank_dam.service')->getApiUrl();

    $libraries['connector']['js'][$protocol . '://' . $url . '/connector/qbank-connector.min.js'] = [
      'type'     => 'external',
      'minified' => TRUE,
    ];
  }
}

function qbank_dam_entity_delete($entity) {
  if ($entity->getEntityTypeId() === 'node') {
    \Drupal::service('qbank_dam.service')->deleteUsage($entity);
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function qbank_dam_node_presave(Drupal\Core\Entity\EntityInterface $entity) {

}

/**
 * Implements hook_preprocess_HOOK().
 */
function qbank_dam_preprocess_page(&$variables) {
  $variables['#attached']['library'][] = 'qbank_dam/qbank_resize';
} 


/**
 * Implements hook_ENTITY_TYPE_postsave().
 */
function qbank_dam_node_postsave(EntityInterface $entity, $op) {

  $qbank_module_name = 'qbank_dam';
  $entity_type = $entity->getEntityTypeId();
  $bundle = $entity->bundle();
  
  //Get Form Display related to the entity bundle
  $form_display = \Drupal::entityTypeManager()
    ->getStorage('entity_form_display')
    ->load('node' . '.' . $bundle . '.' . 'default');

  $field_definitions = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions($entity_type);

  $fields_refering_qbank = [];

  foreach ($field_definitions as $field_name => $value) {
    \Drupal::logger('qbank_dam')->notice('qbank_dam_ENTITY_postsave field : '.$field_name);

    $widget_type = $form_display->getComponent($field_name);
    \Drupal::logger('qbank_dam')->notice('qbank_dam_ENTITY_postsave widget type : '.$widget_type);

    if (isset($widget_type['settings']['entity_browser'])){
      \Drupal::logger('qbank_dam')->notice('qbank_dam_ENTITY_postsave field defined as entity browser widget');
      $widget_id = $widget_type['settings']['entity_browser'];
      //Getting Widget Configuration by id
      $config = \Drupal::config('entity_browser.browser.'.$widget_id);
      $module_configuration = $config->getRawData();

      if (in_array($qbank_module_name, $module_configuration['dependencies']['module'])){
        \Drupal::logger('qbank_dam')->notice('qbank_dam_ENTITY_postsave field '.$qbank_module_name.' dependant field adding to qbank field array');
        $fields_refering_qbank[] = $field_name;
      } 
    }else{
      \Drupal::logger('qbank_dam')->notice('qbank_dam_ENTITY_postsave field is not a entity browser widget');
    }
  }
  
  //Adding fields to capture usage stats
  \Drupal::service('qbank_dam.service')->addUsage($entity, $bundle, $fields_refering_qbank);
}
